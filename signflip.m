loaddata = load('data/david2.mat');

tri = loaddata.surface.TRIV;
X = loaddata.surface.X;
Y = loaddata.surface.Y;
Z= loaddata.surface.Z;

vert = [X Y Z];
n_tri = length(tri);
n = length(vert);
%% Distance graph
S = zeros(n ,n);
S_r = zeros(n ,n);

edgeSum = 0;
for i = 1:n_tri
    v = tri(i, :);
    edgeSum = edgeSum + norm(vert(v(1), :) - vert(v(2), :));
    edgeSum = edgeSum + norm(vert(v(1), :) - vert(v(3), :));
    edgeSum = edgeSum + norm(vert(v(3), :) - vert(v(2), :));
end
edgeAvg = edgeSum/(3*n_tri);
for i = 1:n_tri
    v = tri(i, :);
    S_r(v(1), v(2)) = exp(-(1 + normrnd(0, 0.005))*norm(vert(v(1), :) - vert(v(2), :))/(edgeAvg * 1));
    S(v(1), v(2)) = exp(-norm(vert(v(1), :) - vert(v(2), :))/(edgeAvg * 1));
    S_r(v(2), v(1)) = S_r(v(1), v(2));
    S(v(2), v(1)) = S(v(1), v(2));
    S_r(v(1), v(3)) = exp(-(1 + normrnd(0, 0.005))*norm(vert(v(1), :) - vert(v(3), :))/(edgeAvg * 1));
    S(v(1), v(3)) = exp(-norm(vert(v(1), :) - vert(v(3), :))/(edgeAvg * 1));
    S_r(v(3), v(1)) = S_r(v(1), v(3));
    S(v(3), v(1)) = S(v(1), v(3));
    S_r(v(3), v(2)) = exp(-(1 + normrnd(0, 0.005))*norm(vert(v(3), :) - vert(v(2), :))/(edgeAvg * 1));
    S(v(3), v(2)) = exp(-norm(vert(v(3), :) - vert(v(2), :))/(edgeAvg * 1));
    S_r(v(2), v(3)) = S_r(v(3), v(2));
    S(v(2), v(3)) = S(v(3), v(2));
end

%% Spec clustering

[eivec_r, eival_r, ~] = unormspecclust(S_r, 3400, 'stay');
[eivec, eival, ~] = unormspecclust(S, 3400, 'stay');
%% coloring
usevec = eivec;
account = [4 2];
color = zeros(n, 1);
type = 1; % 0 for plot, 1 for 3D color
for i = account
    color = color + (usevec(:, i)>0);
    color = color*2;
end

if type == 0
%     figure,
    plot(eivec(:, account(1)), eivec(:, account(2)), '.');
else
%     figure,
    trisurf(tri, X, Y, Z, color);
    colormap('jet');
    % colormap([0 0 1;1 0 0]);
    daspect([1 1 1]);
    view(0, 0);
end
%% performance
num_iter = 1;
xaxis = 2:100;
err = zeros(1, length(xaxis));
s_err = zeros(1, length(xaxis));
for k = xaxis
    for itr = 1:num_iter
        %% random sign change
        
%         k = 10;
        refvec = eivec(:, 2:(k+1));
        scvec = eivec_r(:, 2:(k+1));
        for i = 2:k
            scvec(:, i) = scvec(:, i)*sign(rand()-0.5);
        end
        S1 = diag(ones(1, k));
        S2 = diag(ones(1, k));
        %% compute S
        for i = 2:k
            if sum(refvec(refvec(:, i) > 0, 1)) < 0
                S1(i, i) = -1;
            end
            if sum(scvec(scvec(:, i) > 0, 1)) < 0
                S2(i, i) = -1;
            end
        end
        %% compute error
        
        dif = abs(refvec*S1 - scvec*S2);
        s_dif = abs(sign(refvec*S1) - sign(scvec*S2));
        indx = find(xaxis == k);
        err(indx) = err(indx) + sum(dif(:));
        s_err(indx) = s_err(indx) + sum(s_dif(:));
        % wrong_cnt = sum(sum(dif) ~= 0);
        % rat = wrong_cnt/k;
        % find(sum(dif) ~= 0)
        % rat*100
    end
end

subplot(2, 1, 1);
plot(xaxis, err, 'b');
subplot(2, 1, 2);
plot(xaxis, log10(s_err + 1), 'r');