function [y, yval, Lret] = unormspecclust(S, k, type, arg)
    run('~/matlab/extensions/vlfeat-0.9.20/toolbox/vl_setup');
    n = length(S);
        
    %% construct adjacency graph
    W = zeros(n);
    
    % epsilon neighbourhood
    if strcmp(type, 'eps')
        eps = arg;
        W = double(S <= eps);
    % k-nearest
    elseif strcmp(type, 'knear')
        kn = arg;
        % flag decides if the type of knn is mutual
        flg = 0;
        if kn < 0
            flg = 1;
            kn = -kn;
        end
        for i = 1:n
            [~, ind] = sort(S(i,:));
            for j = ind(1 : (kn+1))
                if i ~= j
                    W(i, j) = exp(-S(i, j));
                end
            end
        end
        for i = 1:n
            for j = 1:n
                if i ~= j && W(i, j) == 0 && W(j, i) ~= 0
                    if flg
                        W(j, i) = 0;
                    else
                        W(i, j) = W(j, i);
                    end
                end
            end
        end
        
    % fully connected graph
    elseif strcmp(type, 'fullc')
        sig = arg;
        W = gaussmf(S, [sig, 0]);
    % the input S is the similarity matrix to be used
    else
        W = S;
    end

    %% degree and laplacian matrix
    D = diag(sum(W, 2));
    L = D - W;
    
    %% eigs and kmeans
    [V, D] = eig(L);
    D = diag(D);
    [D, ind] = sort(D);
    V = V(ind, :);
    V = V(:, 1:k);
% [V, D] = eigs(L, k, 'sm');
    
    donorm = 0;
    
    if donorm
        for i = 1:n
            V(i, :) = V(i, :)/norm(V(i, :));
        end
    end
    
    %[~, y] = vl_kmeans(V', k);
    yval = D;
    y = V;
    
    if nargin == 3
        Lret = L;
    end
    
end
