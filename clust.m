loaddata = load('data/david2.mat');
%run('~/matlab/extensions/vlfeat-0.9.20/toolbox/vl_setup');

tri = loaddata.surface.TRIV;
X = loaddata.surface.X;
Y = loaddata.surface.Y;
Z= loaddata.surface.Z;

vert = [X Y Z];
n_tri = length(tri);
n = length(vert);
%% Distance graph
S = zeros(n ,n);

edgeSum = 0;
for i = 1:n_tri
    v = tri(i, :);
    edgeSum = edgeSum + norm(vert(v(1), :) - vert(v(2), :));
    edgeSum = edgeSum + norm(vert(v(1), :) - vert(v(3), :));
    edgeSum = edgeSum + norm(vert(v(3), :) - vert(v(2), :));
end
edgeAvg = edgeSum/(3*n_tri);
for i = 1:n_tri
    v = tri(i, :);
    S(v(1), v(2)) = exp(-norm(vert(v(1), :) - vert(v(2), :))/(edgeAvg * 1));
    S(v(2), v(1)) = S(v(1), v(2));
    S(v(1), v(3)) = exp(-norm(vert(v(1), :) - vert(v(3), :))/(edgeAvg * 1));
    S(v(3), v(1)) = S(v(1), v(3));
    S(v(3), v(2)) = exp(-norm(vert(v(3), :) - vert(v(2), :))/(edgeAvg * 1));
    S(v(2), v(3)) = S(v(3), v(2));
end

%% Spec clustering
[eivec, eival] = unormspecclust(S, 3400, 'stay');
% scatter3(eivec(:, 4), eivec(:, 2), eivec(:, 3));

%% Draw objects side-by-side
drawOrder = [2 3 4];
assign = eivec(:, drawOrder);
numDraw = length(drawOrder);
zero_thresh = 0;
ftri = ones(numDraw * n_tri, 3);
fX = zeros(numDraw * n, 1);
fY = repmat(Y, numDraw, 1);
fZ = repmat(Z, numDraw, 1);
fC = zeros(numDraw * n, 1);

for i = 1:numDraw
    ftri((i-1)*n_tri + (1:n_tri), :) = tri + (i-1)*n;
    fX((i-1)*n + (1:n)) = X + (i-1)*1.05*(max(X) - min(X));
%     [~, colors] = vl_kmeans(assign(:, i)', 2);
% colors = (assign(:, i) - min(assign(:, i)))/range(assign(:, i));
    colors = (assign(:, i) > zero_thresh) - (assign(:, i) < -zero_thresh);
    fC((i-1)*n + (1:n)) = colors;
end
figure,
trisurf(ftri, fX, fY, fZ, fC);
% colormap('jet');
daspect([1 1 1]);
% %% GMM
% numClust = 7;
% gmmodel = fitgmdist(assign, 7, 'Options', statset('MaxIter', 500), 'Replicates', 10);%, 'NumIterations', 2);
% gmm_prob = gmmodel.posterior(assign)';
% %[~, ~, ~, ~, gmm_prob] = vl_gmm(assign', numClust);
% [~, gmm_prob] = max(gmm_prob);
% trisurf(tri, X, Y,Z, gmm_prob');
